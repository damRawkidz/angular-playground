import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy } from '@angular/core';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from './app.service';
import { registerLocaleData } from '@angular/common';
import { fromEvent, Observable, throwError, Subject, BehaviorSubject } from 'rxjs';
import { map, debounceTime, tap, takeUntil, mergeMap, switchMap, concatMap, delay, exhaustMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit,OnDestroy{

  ngOnDestroy(): void {
    this.unSubAll$.next(true)
    this.unSubAll$.complete()
  }

  title = 'angular-playground';
  text$ = new Observable<string>();

  private unSubAll$ = new Subject();

  @ViewChild('input',{static: true}) myInput: ElementRef<HTMLInputElement>

  array = [1,2,3,4,5,6,7]
  initState: number = 0
  form: FormGroup

  // displayMonths = 2;
  // navigation = 'select';
  // showWeekNumbers = false;
  // outsideDays = 'visible';
  // hoveredDate: NgbDate;

  // fromDate: NgbDate;
  // toDate: NgbDate;
  // data = {name:'',age:0}
  subject$ = new Subject()
  onDes$  = new Subject()
  be$ = new BehaviorSubject(null)

  constructor(
      public appSV:AppService,
      public fb:FormBuilder
      // public changeSV: AppService
    ) {
      this.form  = this.fb.group({
        name:['',Validators.required],
        lastname:'',
        addr: this.fb.array([
          this.creatFormArray()
        ])
      })
    }

    creatFormArray(){      
      return this.fb.group({
        addrName:''
      })
    }

    addFormArray(){      
      let formArray  = this.form.get('addr') as FormArray
      formArray.push(this.creatFormArray())
      // this.form.get('name').setValue('test')
      
      // let value =  this.form.get('name').value 
      // this.form.value
      // alert(value)
    }

    removeFormArray(index: number) {
      let formArray  = this.form.get('addr') as FormArray
      formArray.removeAt(index)
    }


    ngOnInit(): void {

        this.form.get('name').valueChanges.pipe(
          debounceTime(1000),
          tap(key => console.log(key)),
          map((key: string) => key.toUpperCase()),
          tap(key => console.log(key)),
          takeUntil(this.unSubAll$)
        ).subscribe()



      this.form.get('addr').valueChanges.pipe(
        tap(result => console.log(result)),
        takeUntil(this.unSubAll$)
      ).subscribe()










      //  let myObserav$ = this.subject$.pipe(
      //   map( (x:number) => x),
      //   exhaustMap(x => {
      //     return this.appSV.mockData(x).pipe(           
      //       delay(2000)
      //     )
      //   })
      // )
      

      // myObserav$.pipe(
      //   tap(x => console.log(x))
      // ).subscribe()
        
        
         
        // let observer$ = new Observable(sub => {
        //   sub.next(1)
        //   sub.next(2)                    
        //   sub.next(3)
        //   sub.complete()
        // })
        





        // observer$.subscribe({
        //   next(x) { console.log('got value ' + x); },

        // });


















      //  this.text$ = fromEvent(this.myInput.nativeElement,'input').pipe(
      //   map( (event:any) => {
      //     return event.target.value
      //   }),
      //   tap(x => console.log('log from Tap',x)),
      //   tap(x => this.appSV.sendText(x)),
      //   debounceTime(1000),
      //   tap(x => console.log('after debount',x))
      // )      
    }
    check(index,checked){
      let formArray = this.form.get('addr') as FormArray
      if(checked){        
        formArray.at(index).get('isbrasdadsd').setValue(checked)
      } else {
        formArray.at(index).get('isbrasdadsd').setValue(checked)
      }
    }


    nextS(){
      this.initState ++ 
      this.subject$.next(this.initState)
    }

    unsub(){
      this.onDes$.next(true)
      this.onDes$.complete()
    }




    eventSub1(event){
      this.title = event
    }

      next(){
        // this.changeSV.next()
        // this.data.name= 'tst'
        // this.data.age = 100
        // let obj = {...this.data}
        // this.data = obj
        // let obj:any = {...this.data,age:10,name:'test'}        
        // this.data = obj;
        // this.changeSV.change$.next('call from com1')
      }


      suffleArray(){
        console.log(
          this.array.sort(() =>  0.5 - Math.random())
        )
      }
















  // onDateSelection(date: NgbDate) {
  //   if (!this.fromDate && !this.toDate) {
  //     this.fromDate = date;
  //   } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
  //     this.toDate = date;
  //   } else {
  //     this.toDate = null;
  //     this.fromDate = date;
  //   }
  // }

  // isHovered(date: NgbDate) {
  //   return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  // }

  // isInside(date: NgbDate) {
  //   return date.after(this.fromDate) && date.before(this.toDate);
  // }

  // isRange(date: NgbDate) {
  //   return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  // }
}
