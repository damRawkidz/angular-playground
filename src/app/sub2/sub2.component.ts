import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-sub2',
  templateUrl: './sub2.component.html',
  styleUrls: ['./sub2.component.css']
})
export class Sub2Component implements OnInit {
  // @Input() titleSub2:string = ''
  public sub2$ = new Observable<String>()
  constructor(
    public appSV:AppService
  ) { 
    let array = this.twoSum([3,2,4],5)
    console.log(array)
  }

  ngOnInit() {
    this.sub2$ = this.appSV.getTextObserver().pipe(
      map(x => {
        return `Change from sub 2 ${x.toLowerCase()}`
      })
    )
  }




  
  twoSum(nums,target){
    let answes = [0,0]
    let firstPosition = 0
    let secposition = 0
    for(let currIndex = 0; currIndex < nums.length; currIndex++ ){
        
        
        console.log(firstPosition)
        for(let secIndex = 0; secIndex < nums.length; secIndex++ ){

          
            if(nums[firstPosition] + nums[secIndex + 1] == target){

                answes[0] = firstPosition;
                answes[1] = secIndex + 1;                
                return answes
                
            } else {

              firstPosition ++
            }
        }
            
    }   
}
}