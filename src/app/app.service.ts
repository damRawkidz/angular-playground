import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, debounceTime, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private textSV$ = new BehaviorSubject<string>('')
  mock = [1,2,3,4,5,6,7,8,50]
  sendText(text:string){
    this.textSV$.next(text)
  }

  getTextObserver(): Observable<String> {
    return this.textSV$.asObservable().pipe(
      map(x => x.toUpperCase())
    )
  }

  mockData(num:Number){
    let item = this.mock.find(x => x === num)
    return of({data:item})
  }










  // private data = {name:'',age:0}
  // change$ = new BehaviorSubject<any>({name:'',age:0})







    constructor() { }

  // next(){
  //   console.log({name:'next',age:Math.random()})
  //   // this.change$.next({...this.data,
  //   //   name: 'next',
  //   //   age: Math.random()
  //   // })
  //   this.change$.next({
  //     name: 'next',
  //     age: Math.random()
  //   })
  // }
}
